# Docker Container to run Collectd-web

# Use

Prebuilt image:

```
rsync -avz host:/var/lib/collectd/rrd/ /tmp/rrd/
docker run -p 8080:80 --volume /tmp:/tmp -it registry.gitlab.com/opsone_ch/docker-collectd-web:latest
```

# Build

Build yourself:

```
docker build -t ops-collectdweb .
```